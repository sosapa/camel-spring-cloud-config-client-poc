

package au.com.redenergy.springcloudconfig


import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class ConfigClientApplication {

  static void main(String[] args) {
    SpringApplication.run(ConfigClientApplication.class, args)
  }
}

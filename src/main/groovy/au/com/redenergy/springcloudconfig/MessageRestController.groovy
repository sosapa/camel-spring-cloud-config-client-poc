
package au.com.redenergy.springcloudconfig

import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RefreshScope
@RestController
class MessageRestController {

  @Value("\${message:suck it can't find it default}")
  private String message

  @Value("\${secret.message:suck it can't find it default}")
  private String secretMessage

  @RequestMapping("/message")
  String getMessage() {
    return this.message
  }

  @RequestMapping("/message/secret")
  String getSecretMessage() {
    return this.secretMessage
  }
}